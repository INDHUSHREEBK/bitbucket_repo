const { chromium } = require('playwright')
const { expect } = require('@playwright/test');
const cp = require('child_process');
const playwrightClientVersion = cp.execSync('npx playwright --version').toString().trim().split(' ')[1];
const { I } = inject();

(async () => {
  const capabilities = {
    'browserName': 'pw-chromium', // Browsers allowed: `Chrome`, `MicrosoftEdge`, `pw-chromium`, `pw-firefox` and `pw-webkit`
    'browserVersion': 'latest',
    'LT:Options': {
      'platform': 'Windows 10',
      'build': 'pw101 assignment1',
      'name': 'pw101 assignment1',
      'user': process.env.LT_USERNAME,
      'accessKey': process.env.LT_ACCESS_KEY,
      'network': true,
      'video': true,
      'console': true,
      'tunnel': false, // Add tunnel configuration if testing locally hosted webpage
      'tunnelName': '', // Optional
      "geoLocation": "IN", // country code can be fetched from https://www.lambdatest.com/capabilities-generator/
      'playwrightClientVersion': playwrightClientVersion
    }
  }

  const browser = await chromium.connect({
    wsEndpoint: `wss://cdp.lambdatest.com/playwright?capabilities=${encodeURIComponent(JSON.stringify(capabilities))}`
  })

  const page = await browser.newPage()

  await page.goto('https://www.lambdatest.com/selenium-playground')
  const simpleFormDemoLink = await page.$('//a[text()="Simple Form Demo"]');
  await simpleFormDemoLink.click({delay: 500});
  try {
    await  expect(page).toHaveURL("https://www.lambdatest.com/selenium-playground/simple-form-demo");
  }
  catch {
      await page.evaluate(_ => {}, `lambdatest_action: ${JSON.stringify({ action: 'setTestStatus', arguments: { status: 'failed', remark: 'URL Title not matched' } })}`)
  }  
  const text = "Welcome to LambdaTest";
  const messageBox = await page.$('input#user-message');
  await messageBox.type(text, {delay: 50});
  const checkedValButton = await page.$('button#showInput');
  await checkedValButton.click();
  const yourMessage = await page.$('p#message');
  try {
  await expect(await yourMessage.textContent()).toEqual(text);   
  await I.executeScript('lambda-status=passed')
  }
  catch {
    await page.evaluate(_ => {}, `lambdatest_action: ${JSON.stringify({ action: 'setTestStatus', arguments: { status: 'failed', remark: 'Message not matched' } })}`)
 }
await browser.close();

  // try {
  //   expect(title).toEqual('LambdaTest - Search')
  //   // Mark the test as completed or failed
  //   await page.evaluate(_ => {}, `lambdatest_action: ${JSON.stringify({ action: 'setTestStatus', arguments: { status: 'passed', remark: 'Title matched' } })}`)
  // } catch {
  //   await page.evaluate(_ => {}, `lambdatest_action: ${JSON.stringify({ action: 'setTestStatus', arguments: { status: 'failed', remark: 'Title not matched' } })}`)
  // }
})()
